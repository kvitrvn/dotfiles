STOW=stow -v -t ~

.PHONY: install
install:
	xargs -d '\n' -a packages/package.list pacman --needed -Su
	
installAUR:
	xargs -d '\n' -a packages/aur.list yay --needed -Su

installi3: install
	xargs -d '\n' -a packages/i3.list pacman --needed -Su

.PHONY: i3
i3: installi3
	$(STOW) Xresources
	xrdb ~/.Xresources
	$(STOW) i3
	$(STOW) git
	$(STOW) polybar
	$(STOW) bash
	$(STOW) code